using UnityEngine;
using Core.SaveSystem;

public class MainSceneController : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField] private UI.MainView MainView;
    #endregion

    #region Private Fields
    private const string CONST_FileName = "/KJTest.json";

    private SaveController saveController;
    #endregion

    private void Awake()
    {
        string dataPath = Application.persistentDataPath + CONST_FileName;

        ISaveFileProvider saveFileProvider = new JsonSaveFileProvider(dataPath);
        //ISaveFileProvider saveFileProvider = new CloudSaveFileProvider();

        saveController = new SaveController(saveFileProvider);
    }

    void Start()
    {
        MainView.OnLoad = saveController.Load;
        MainView.OnSave = (name, age, isPremium) =>
        {
            saveController.SetValue("Name", name);
            saveController.SetValue("Age", age);
            saveController.SetValue("Premium", isPremium);

            saveController.Save();
        };

        MainView.Init(
            () => saveController.GetValue<string>("Name"),
            () => saveController.GetValue<double>("Age"),
            () => saveController.GetValue<bool>("Premium")
            );
    }
}
