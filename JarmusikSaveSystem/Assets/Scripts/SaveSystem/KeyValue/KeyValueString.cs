[System.Serializable]
public class KeyValueString
{
    public string Key;
    public string Value;
    public KeyValueString(string key, string value)
    {
        Key = key;
        Value = value;
    }
}