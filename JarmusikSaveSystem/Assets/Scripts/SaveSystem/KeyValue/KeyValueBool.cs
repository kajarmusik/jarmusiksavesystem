[System.Serializable]
public class KeyValueBool
{
    public string Key;
    public bool Value;

    public KeyValueBool(string key, bool value)
    {
        Key = key;
        Value = value;
    }
}