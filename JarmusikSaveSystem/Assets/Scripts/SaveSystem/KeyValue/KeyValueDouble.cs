[System.Serializable]
public class KeyValueDouble
{
    public string Key;
    public double Value;
    public KeyValueDouble(string key, double value)
    {
        Key = key;
        Value = value;
    }
}
