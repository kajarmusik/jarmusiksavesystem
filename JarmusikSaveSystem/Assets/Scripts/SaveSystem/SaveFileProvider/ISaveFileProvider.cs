namespace Core.SaveSystem
{
    public interface ISaveFileProvider
    {
        void Save(SaveController saveController);
        void Load(SaveController saveController);
    }
}