using System.Collections.Generic;
using UnityEngine;
namespace Core.SaveSystem
{
    public class JsonSaveFileProvider : ISaveFileProvider
    {
        List<KeyValueDouble> listKeyValueFloat = new List<KeyValueDouble>();
        List<KeyValueString> listKeyValueString = new List<KeyValueString>();
        List<KeyValueBool> listKeyValueBool = new List<KeyValueBool>();

        private string jsonPath = "";
        public JsonSaveFileProvider(string jsonPath)
        {
            this.jsonPath = jsonPath;
        }

        public void Load(SaveController saveController)
        {
            if (!System.IO.File.Exists(jsonPath))
            {
                return;
            }

            string jsonString = System.IO.File.ReadAllText(jsonPath);
            KeyValueArrays keyValueItems = JsonUtility.FromJson<KeyValueArrays>(jsonString);

            saveController.dictionaries.Clear();

            foreach (var item in keyValueItems.DoubleItems)
            {
                saveController.SetValue(item.Key, item.Value);
            }
            foreach (var item in keyValueItems.StringItems)
            {
                saveController.SetValue(item.Key, item.Value);
            }
            foreach (var item in keyValueItems.BoolItems)
            {
                saveController.SetValue(item.Key, item.Value);
            }
        }

        public void Save(SaveController saveController)
        {
            Dictionary<string, object> keyValuePairs;

            if (saveController.dictionaries.TryGetValue(typeof(double), out keyValuePairs))
            {

                foreach (var item in keyValuePairs)
                {
                    KeyValueDouble keyValueFloat = new KeyValueDouble(item.Key, (double)item.Value);
                    listKeyValueFloat.Add(keyValueFloat);
                }
                keyValuePairs.Clear();
            }

            if (saveController.dictionaries.TryGetValue(typeof(string), out keyValuePairs))
            {

                foreach (var item in keyValuePairs)
                {
                    KeyValueString keyValueString = new KeyValueString(item.Key, (string)item.Value);
                    listKeyValueString.Add(keyValueString);
                }
                keyValuePairs.Clear();
            }

            if (saveController.dictionaries.TryGetValue(typeof(bool), out keyValuePairs))
            {

                foreach (var item in keyValuePairs)
                {
                    KeyValueBool keyValueBool = new KeyValueBool(item.Key, (bool)item.Value);
                    listKeyValueBool.Add(keyValueBool);
                }
                keyValuePairs.Clear();
            }

            KeyValueArrays saveItems = new KeyValueArrays(listKeyValueFloat.ToArray(), listKeyValueString.ToArray(), listKeyValueBool.ToArray());

            string jsonString = JsonUtility.ToJson(saveItems);
            System.IO.File.WriteAllText(jsonPath, jsonString);

            listKeyValueFloat.Clear();
            listKeyValueString.Clear();
            listKeyValueBool.Clear();
        }
    }
}
