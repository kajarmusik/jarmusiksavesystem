using UnityEngine;

namespace Core.SaveSystem
{
    public class CloudSaveFileProvider : ISaveFileProvider
    {
        public CloudSaveFileProvider() { }
        public void Load(SaveController saveController)
        {
            Debug.Log("Cloud Load");
        }
        public void Save(SaveController saveController)
        {
            Debug.Log("Cloud Save");
        }
    }
}