
[System.Serializable]
public class KeyValueArrays
{
    public KeyValueDouble[] DoubleItems;
    public KeyValueString[] StringItems;
    public KeyValueBool[] BoolItems;
    public KeyValueArrays(KeyValueDouble[] doubleItems, KeyValueString[] stringItems, KeyValueBool[] boolItems)
    {
        DoubleItems = doubleItems;
        StringItems = stringItems;
        BoolItems = boolItems;
    }
}