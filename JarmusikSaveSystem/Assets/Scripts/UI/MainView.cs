using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.Collections;

namespace UI
{
    public class MainView : MonoBehaviour
    {
        public Action<string, double, bool> OnSave;
        public Action OnLoad;

        #region Serialized Fields
        [SerializeField] private TMP_InputField nameInputField;
        [SerializeField] private Slider ageSlider;
        [SerializeField] private TextMeshProUGUI ageLabel;
        [SerializeField] private Toggle premiumToggle;

        [SerializeField] private Transform checkmark;
        #endregion

        private Func<string> GetPlayerName;
        private Func<double> GetAge;
        private Func<bool> GetIsPremium;

        private const float CONST_MaxTimeCheckmarksAnim = .3f;
        private const float CONST_CheckmarkScaleMultiplier = 1.5f;

        private WaitForSeconds delayHideCheckmark = new WaitForSeconds(1.0f);
        private Coroutine checkmarkAnimationCoroutine;
        public void Init(Func<string> getPlayerName, Func<double> age, Func<bool> isPremium)
        {
            GetPlayerName = getPlayerName;
            GetAge = age;
            GetIsPremium = isPremium;

            ageSlider.onValueChanged.AddListener(OnAgeChanged);

            FillContent();
        }
        public void OnSaveBtnClicked()
        {
            OnSave?.Invoke(nameInputField.text, ageSlider.value, premiumToggle.isOn);

            if (checkmarkAnimationCoroutine != null)
                StopCoroutine(checkmarkAnimationCoroutine);
            checkmarkAnimationCoroutine = StartCoroutine(ShowCheckmark());
        }
        public void OnLoadBtnClicked()
        {
            OnLoad?.Invoke();

            FillContent();

            if (checkmarkAnimationCoroutine != null)
                StopCoroutine(checkmarkAnimationCoroutine);
            checkmarkAnimationCoroutine = StartCoroutine(ShowCheckmark());
        }
        public void FillContent()
        {
            nameInputField.text = GetPlayerName();
            ageSlider.value = (float)GetAge();
            ageLabel.text = GetAge().ToString();
            premiumToggle.isOn = GetIsPremium();
        }
        public void DeInit()
        {
            ageSlider.onValueChanged.RemoveListener(OnAgeChanged);
        }

        private void OnAgeChanged(float age)
        {
            ageLabel.text = age.ToString();
        }

        private IEnumerator ShowCheckmark()
        {
            float time = 0;

            checkmark.gameObject.SetActive(true);

            while (time < CONST_MaxTimeCheckmarksAnim)
            {
                yield return null;

                checkmark.localScale = Vector3.Lerp(Vector3.one, Vector3.one * CONST_CheckmarkScaleMultiplier, time / CONST_MaxTimeCheckmarksAnim);
                time += Time.deltaTime;
            }

            time = 0;

            while (time < CONST_MaxTimeCheckmarksAnim)
            {
                yield return null;

                checkmark.localScale = Vector3.Lerp(checkmark.localScale, Vector3.one, time / CONST_MaxTimeCheckmarksAnim);
                time += Time.deltaTime;
            }
            checkmark.localScale = Vector3.one;

            yield return delayHideCheckmark;

            checkmark.gameObject.SetActive(false);
        }
    }
}
