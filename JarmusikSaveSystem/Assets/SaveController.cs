using System.Collections.Generic;

namespace Core.SaveSystem
{
    public class SaveController : ISaveController
    {
        private ISaveFileProvider SaveFileProvider;
        public SaveController(ISaveFileProvider saveFileProvider)
        {
            SaveFileProvider = saveFileProvider;
        }

        public Dictionary<System.Type, Dictionary<string, object>> dictionaries = new Dictionary<System.Type, Dictionary<string, object>>();

        public void SetValue<T>(string key, T value)
        {
            var type = typeof(T);
            if (dictionaries.TryGetValue(type, out var dictionary))
            {
                if (dictionary.ContainsKey(key))
                {
                    dictionary[key] = value;
                }
                else
                {
                    dictionary.Add(key, value);
                }
            }
            else
            {
                var newDictionary = new Dictionary<string, object>();
                newDictionary.Add(key, value);
                dictionaries.Add(type, newDictionary);
            }
        }

        public T GetValue<T>(string key)
        {
            var type = typeof(T);
            if (dictionaries.TryGetValue(type, out var dictionary))
            {
                if (dictionary.TryGetValue(key, out var result))
                {
                    return (T)result;
                }
            }
            return default(T);
        }

        public void Save()
        {
            SaveFileProvider.Save(this);
        }

        public void Load()
        {
            SaveFileProvider.Load(this);
        }
    }
}